[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/ch-mc-b/cdi/infra/gitpod-workspace)

GitPod Workspace
================

Angepasste Workspace für GitPod inkl. Docker, kubectl, terraform und WireGuard

Basierend auf der Anleitung [Custom Docker Image](https://www.gitpod.io/docs/config-docker).

Erstellen eines benutzerdefinierten Dockerfile(s)
-------------------------------------------------

Erstellt eine `.gitpod.yml` Datei mit folgendem Inhalt

    image:
        file: .gitpod.Dockerfile

Und eine dazugehörendes Dockerfile mit Namen `.gitpod.Dockerfile`

Kopiert den Inhalt laut [.gitpod.Dockerfile](https://gitlab.com/mc-b-cdi/infra/gitpod-workspace/-/blob/main/.gitpod.Dockerfile).

Testen
------

Gitpod Umgebung starten, z.B. über `Gitpod` Button oben in der Repository Leiste.

    docker build -f .gitpod.Dockerfile -t gitpod-dockerfile-test .

    docker run -it gitpod-dockerfile-test bash    

Funktioniert das Build des Container Images ohne Probleme, kann in der gestarteten `bash` die neu installieren Programme ausprobiert werden:

    terraform 
    az
    aws
    wg-quick
    kubectl

Konfiguration Workspace
-----------------------

Diese erfolgt in der [Gitpod](https://gitpod.io/variables) Oberfläche.

Folgende Variablen werden unterstützt:
* K8S_CONFIG - Kubernetes `.kube/config` Datei im Base64 Format. 
* WG_CONFIG - WireGuard Konfiguration im Base64 Format
* AWS_CONFIG_FILE - zeigt auf `/workspace/.aws-config`. Inhalt manuell aus AWS Academy kopieren.

Um die Dateien wie `.kube/config`, im Base64 Format, aufzubereiten ist folgender Befehl zu verwenden:

    cat  ~/.kube/config | base64 -w 0

**ACHTUNG:** bei Verwendung von VPNs (z.B. WireGuard) ist zuerst die IP-Adresse im `server:` Eintrag zu ändern.

Die WG_CONFIG wird als `/workspace/.wg-config.conf` Datei abgelegt. WireGuard kann wie folgt aktiviert werden:

    wg_quick up /workspace/.wg-config.conf

Um aus den Variablen Dateien zu erstellen, ist `.gitpod.yml` wie folgt zu Erweitern:

    tasks:
    - command: echo "$WG_CONFIG"  | base64 -d > /workspace/.wg-config.conf
    - command: echo "$K8S_CONFIG" | base64 -d > ~/.kube/config
    - command: touch "$AWS_CONFIG_FILE"





