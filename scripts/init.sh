#!/bin/bash

cat <<%EOF% >$HOME/.ssh/config
StrictHostKeyChecking no
UserKnownHostsFile /dev/null
LogLevel error
User ubuntu
%EOF%

# Alias
alias tf='terraform'
alias tfa='terraform apply -auto-approve'
alias tfd='terraform destroy -auto-approve'
alias tfi='terraform init'
alias tfo='terraform output'
alias tfp='terraform plan'

cat <<%EOF% >>$HOME/.bashrc
alias tf='terraform'
alias tfa='terraform apply -auto-approve'
alias tfd='terraform destroy -auto-approve'
alias tfi='terraform init'
alias tfo='terraform output'
alias tfp='terraform plan'

export CDPATH=${GITPOD_REPO_ROOT}/cdi/infra:${GITPOD_REPO_ROOT}/cdi/apps:${GITPOD_REPO_ROOT}:$CDPATH
%EOF%

export CDPATH=${GITPOD_REPO_ROOT}/cdi/infra:${GITPOD_REPO_ROOT}/cdi/apps:${GITPOD_REPO_ROOT}:$CDPATH

# cdi alt
[ -d ${GITPOD_REPO_ROOT}/cdi-jenkins ] && { cd ${GITPOD_REPO_ROOT}/cdi-jenkins; git pull; } || { git clone https://github.com/mc-b/cdi cdi-jenkins; }

cd ${GITPOD_REPO_ROOT}
